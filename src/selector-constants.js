export const APP_TITLE = 'app-title';
export const APP_SUBTITLE = 'app-subtitle';
export const APP_CONTAINER = 'app-container';
export const APP_SECTION = 'app-section';
export const APP_SECTION_BUTTON = `${APP_SECTION}__button`;
export const APP_FOOTER = 'app-footer';

export const JOIN_PROGRAM_SECTION = 'join-program-section';
export const JOIN_PROGRAM_SECTION_BUTTON = `${JOIN_PROGRAM_SECTION}__button`;
export const JOIN_PROGRAM_SECTION_EMAIL = `${JOIN_PROGRAM_SECTION}__email`;
export const JOIN_PROGRAM_SECTION_FORM = `${JOIN_PROGRAM_SECTION}__form`;

export const COMMUNITY_SECTION = 'community-section';
export const COMMUNITY_SECTION_BUTTON = 'community-section__button';
export const COMMUNITY_SECTION_CONTAINER = 'community-section__container';
export const COMMUNITY_SECTION_MEMBERS = 'community-section__members';
export const COMMUNITY_SECTION_MEMBERS_AVATAR =
  'community-section__members__avatar';
export const COMMUNITY_SECTION_MEMBERS_DESCRIPTION =
  'community-section__members__info';
export const COMMUNITY_SECTION_MEMBERS_NAME =
  'community-section__members__name';
export const COMMUNITY_SECTION_MEMBERS_POSITION =
  'community-section__members__position';
