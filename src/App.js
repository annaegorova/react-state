import './App.css';
import { Section } from './components/Section/Section';
import { Community } from './components/Community/Community';
import { JoinUs } from './components/JoinUs/JoinUs';
import { COMMUNITY, JOIN_US } from './text-constants';
import {
  COMMUNITY_SECTION,
  JOIN_PROGRAM_SECTION,
  JOIN_PROGRAM_SECTION_FORM
} from './selector-constants';

function App() {
  return (
    <div className="App">
      <Section
        title={COMMUNITY.title}
        subtitle={COMMUNITY.subtitle}
        classNames={[COMMUNITY_SECTION]}
      >
        <Community className={COMMUNITY_SECTION} />
      </Section>
      <Section
        title={JOIN_US.title}
        subtitle={JOIN_US.subtitle}
        classNames={[JOIN_PROGRAM_SECTION]}
      >
        <JoinUs className={JOIN_PROGRAM_SECTION_FORM} />
      </Section>
    </div>
  );
}

export default App;
