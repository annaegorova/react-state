import { useState } from 'react';
import { postData } from '../../utils/http-utils';
import { validate } from '../../utils/validate';
import {
  APP_SECTION_BUTTON,
  JOIN_PROGRAM_SECTION_BUTTON,
  JOIN_PROGRAM_SECTION_EMAIL,
} from '../../selector-constants';
import './JoinUs.css';

export const JoinUs = ({className}) => {
  const [email, setEmail] = useState('');
  const [subscribed, setSubscribed] = useState(false);
  const [disabled, setDisabled] = useState(false);

  const validateAndSubscribe = async (e) => {
    e.preventDefault();
    if (validate(email)) {
      setDisabled(true);
      try {
        await postData('http://localhost:8080/subscribe', { email });
        setSubscribed(true);
        setEmail('');
      } catch (e) {
        console.log(e);
        if (e.status === 422) {
          alert(`${e.status}: ${e.message}`);
        }
      }
      setDisabled(false);
    }
  };

  const unsubscribeFromProgram = async (e) => {
    e.preventDefault();
    setDisabled(true);
    try {
      await postData('http://localhost:8080/unsubscribe', { email });
      setSubscribed(!subscribed);
    } catch (e) {
      console.log(e);
    }
    setDisabled(false);
  };

  return (
    <form className={className}>
      {!subscribed && <input
        type='text'
        className={JOIN_PROGRAM_SECTION_EMAIL}
        placeholder='Email'
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />}
      <button
        className={`${APP_SECTION_BUTTON} ${JOIN_PROGRAM_SECTION_BUTTON}${disabled ? ' disabled' : ''}`}
        onClick={subscribed ? unsubscribeFromProgram : validateAndSubscribe}
        disabled={disabled}
      >
        {subscribed ? 'Unsubscribe' : 'Subscribe'}
      </button>
    </form>
  );
};
