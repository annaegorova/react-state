import { useEffect, useState } from 'react';
import { getData } from '../../utils/http-utils';
import {
  COMMUNITY_SECTION_BUTTON,
  COMMUNITY_SECTION_CONTAINER,
  COMMUNITY_SECTION_MEMBERS,
  COMMUNITY_SECTION_MEMBERS_AVATAR,
  COMMUNITY_SECTION_MEMBERS_DESCRIPTION,
  COMMUNITY_SECTION_MEMBERS_NAME,
  COMMUNITY_SECTION_MEMBERS_POSITION
} from '../../selector-constants';
import './Community.css';

export const Community = ({ className }) => {
  const [members, setMembers] = useState([]);
  const [displayed, setDisplayed] = useState(true);

  const getMembers = async () => {
    const members = await getData('http://localhost:8080/community/');
    setMembers(members);
  };
  useEffect(() => {
    void getMembers();
  }, []);
  return (
    <div className={className}>
      <button
        className={COMMUNITY_SECTION_BUTTON}
        onClick={() => setDisplayed(!displayed)}
      >
        {`${displayed ? 'Hide' : 'Show'} section`}
      </button>
      <div className={COMMUNITY_SECTION_CONTAINER}>
        {displayed && members.map(member => {
          const fullName = `${member.firstName} ${member.lastName}`;
          return <div
            className={COMMUNITY_SECTION_MEMBERS}
            key={member.firstName}
          >
            <img className={COMMUNITY_SECTION_MEMBERS_AVATAR} src={member.avatar} alt={fullName}/>
            <div className={COMMUNITY_SECTION_MEMBERS_DESCRIPTION}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.
            </div>
            <div className={COMMUNITY_SECTION_MEMBERS_NAME}>
              {fullName}
            </div>
            <div className={COMMUNITY_SECTION_MEMBERS_POSITION}>
              {member.position}
            </div>
          </div>
        })}
      </div>
    </div>
  );
};
