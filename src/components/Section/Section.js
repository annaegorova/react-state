import { APP_SECTION, APP_TITLE, APP_SUBTITLE } from '../../selector-constants';
import './Section.css';

export const Section = ({ title, subtitle, classNames, children }) => {
  return (
    <section className={[...classNames, APP_SECTION].join(' ')}>
      <h2 className={APP_TITLE}>{title}</h2>
      <h3 className={APP_SUBTITLE}>{subtitle}</h3>
      {children}
    </section>
  );
};
