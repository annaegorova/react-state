export const COMMUNITY = {
  title: 'Big Community of\nPeople Like You',
  subtitle: 'We’re proud of our products, and we’re really excited when we get feedback from our users.'
}
 export const JOIN_US = {
  title: 'Join Our Program',
  subtitle: 'Sed do eiusmod tempor incididunt \nut labore et dolore magna aliqua.'
 }
