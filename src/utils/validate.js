const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export function validate(email) {
  return VALID_EMAIL_ENDINGS.some(ending => email.endsWith(ending));
}
