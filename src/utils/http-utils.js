import { CustomError } from '../components/CustomError';

async function makeRequest(url, initObjPartial) {
  const initObj = {
    headers: {
      'Access-Control-Allow-Origin': 'http://localhost:8080',
      'Content-Type': 'application/json;charset=utf-8'
    },
    mode: 'cors',
    ...initObjPartial
  };

  try {
    const response = await fetch(url, initObj);
    const responseData = await response.json();
    const status = response.status;
    if (status >= 400) {
      throw new CustomError(responseData.error, status);
    };
    return responseData;
  } catch (error) {
    throw error;
  }
}

export function postData(url, payload) {
  const initObjPartial = {
    method: 'POST',
    body: JSON.stringify(payload)
  };

  return makeRequest(url, initObjPartial);
}

export function getData(url) {
  const initObjPartial = {
    method: 'GET'
  };
  return makeRequest(url, initObjPartial);
}
